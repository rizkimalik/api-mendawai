'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const report_sentiment_anaysis_grid = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, date_start, date_end } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY date_create DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const result = await knex.raw(`
            SELECT id,message,channel,name,DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create,sentiment,objective_score,positive_score,negative_score FROM view_chats 
            WHERE flag_to='customer' AND agent_handle<>'SPAM' AND DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}'
            ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total from view_chats 
            WHERE flag_to='customer' AND agent_handle<>'SPAM' 
            AND DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('report/report_sentiment_anaysis_grid', error);
        res.status(500).end();
    }
}

const report_sentiment_anaysis_export = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { date_start, date_end } = req.body;
        const a = new Date(date_start);
        const b = new Date(date_end);
        const difference = b.getTime() - a.getTime();
        const days = Math.ceil(difference / (1000 * 3600 * 24)) + 1;
        const max = 62;

        if (days <= max + 1) {
            const result = await knex.raw(`
            SELECT message,channel,name,date_create,sentiment,objective_score,positive_score,negative_score FROM view_chats
                WHERE flag_to='customer' AND agent_handle<>'SPAM' AND DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' ORDER BY date_create DESC
            `);

            response.ok(res, result[0]);
        }
        else {
            res.json({
                'status': 204,
                'data': `Max range ${max} days.`
            });
            res.end();
        }
    }
    catch (error) {
        logger('report/report_sentiment_anaysis_export', error);
        res.status(500).end();
    }
}


module.exports = { report_sentiment_anaysis_grid, report_sentiment_anaysis_export }