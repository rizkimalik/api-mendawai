'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const speed_answer_email = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        // auth_jwt_bearer(req, res);
        const { action } = req.body;

        let date_range = '';
        if (action === 'Today') {
            date_range = 'DATE(dateCreate) = DATE(NOW())';
        }
        else if (action === 'Last7Days') {
            date_range = 'DATE(dateCreate) BETWEEN DATE_SUB(DATE(NOW()), INTERVAL 7 DAY) AND DATE(NOW())';
        }

        const result = await knex.raw(`
            SELECT 
                time_range,
                total_count,
                SUM( total_count ) OVER () AS total_sum,
                ( total_count / SUM( total_count ) OVER () * 100 ) AS percentage
            FROM (
                SELECT '1' AS ROW, '1 Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 0 AND time_diff <= 1 
                UNION ALL
                SELECT '2' AS ROW, '2 Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 1 AND time_diff <= 2 
                UNION ALL
                SELECT '3' AS ROW, '5 Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 2 AND time_diff <= 5 
                UNION ALL
                SELECT '4' AS ROW,  '10 Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 5 AND time_diff <= 10 
                UNION ALL
                SELECT '5' AS ROW,  '15 Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 10 AND time_diff <= 15
                UNION ALL
                SELECT '6' AS ROW,  '20 Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 15 AND time_diff <= 20
                UNION ALL
                SELECT '7' AS ROW,  '24 Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 20 AND time_diff <= 24
                UNION ALL
                SELECT '8' AS ROW,  '24+ Hour' AS time_range, COUNT(*) AS total_count 
                FROM ( 
                    SELECT ticket_number, TIMESTAMPDIFF(HOUR, MIN(date_blending), (SELECT MIN(date_sent) FROM email_out WHERE ticket_number=email_mailbox.ticket_number)) AS time_diff FROM email_mailbox GROUP BY ticket_number
                ) AS dataa WHERE time_diff >= 24
            )  AS speed_answer_email ORDER BY ROW ASC
        `);
        response.ok(res, result[0]);
    }
    catch (error) {
        logger('report/speed_answer_email', error);
        res.status(500).end();
    }
}

module.exports = { speed_answer_email }