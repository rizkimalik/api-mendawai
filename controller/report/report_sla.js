'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { response } = require('../../helper');

const report_sla_grid = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, date_start, date_end, customer_id } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY date_create DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let where_condition = '';
        if (customer_id) {
            where_condition = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND customer_id='${customer_id}' OR status='Open'`;
        }
        else {
            where_condition = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' OR status='Open'`;
        }

        const result = await knex.raw(`
            SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_created, DATE_FORMAT(date_closed,'%Y-%m-%d %H:%i:%s') AS date_closed, DATE_FORMAT(last_response_at,'%Y-%m-%d %H:%i:%s') AS last_response_at FROM v_report_sla 
            ${where_condition} ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from v_report_sla ${where_condition} ${filtering}`);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        response.data(res, error.message, 'report/sla_grid');
    }
}

const report_sla_export = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { date_start, date_end, customer_id } = req.body;
        const a = new Date(date_start);
        const b = new Date(date_end);
        const difference = b.getTime() - a.getTime();
        const days = Math.ceil(difference / (1000 * 3600 * 24)) + 1;
        const max = 62;

        let where_condition = '';
        if (customer_id) {
            where_condition = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' AND customer_id='${customer_id}' OR status='Open'`;
        }
        else {
            where_condition = `WHERE DATE_FORMAT(date_create,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(date_create,'%Y-%m-%d') <= '${date_end}' OR status='Open'`;
        }

        if (days <= max + 1) {
            const result = await knex.raw(`
                SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_created, DATE_FORMAT(date_closed,'%Y-%m-%d %H:%i:%s') AS date_closed, DATE_FORMAT(last_response_at,'%Y-%m-%d %H:%i:%s') AS last_response_at FROM v_report_sla 
                ${where_condition} ORDER BY date_create DESC
            `);
            response.ok(res, result[0]);
        }
        else {
            res.json({
                'status': 204,
                'data': `Max range ${max} days.`
            });
            res.end();
        }
    }
    catch (error) {
        response.data(res, error.message, 'report/sla_export');
    }
}

module.exports = { report_sla_grid, report_sla_export }