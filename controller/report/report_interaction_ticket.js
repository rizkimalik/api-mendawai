'use strict';
const knex = require('../../config/db_connect');
const { auth_jwt_bearer } = require('../../middleware');
const { logger, response } = require('../../helper');

const report_interaction_grid = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, date_start, date_end, customer_id } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY id DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let filter_by_customer = '';
        if (customer_id) {
            filter_by_customer = `AND customer_id='${customer_id}'`;
        }

        const result = await knex.raw(`
            SELECT *, DATE_FORMAT(created_at,'%Y-%m-%d %H:%i:%s') AS created_at FROM v_report_interaction 
            WHERE DATE_FORMAT(created_at,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(created_at,'%Y-%m-%d') <= '${date_end}'
            ${filter_by_customer} ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total from v_report_interaction 
            WHERE DATE_FORMAT(created_at,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(created_at,'%Y-%m-%d') <= '${date_end}' ${filter_by_customer} ${filtering}
        `);
        response.data(res, result[0], total[0][0].total);
    }
    catch (error) {
        logger('report/interaction_grid', error);
        res.status(500).end();
    }
}

const report_interaction_export = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { date_start, date_end, customer_id } = req.body;
        const a = new Date(date_start);
        const b = new Date(date_end);
        const difference = b.getTime() - a.getTime();
        const days = Math.ceil(difference / (1000 * 3600 * 24)) + 1;
        const max = 62;

        let filter_by_customer = '';
        if (customer_id) {
            filter_by_customer = `AND customer_id='${customer_id}'`;
        }

        if (days <= max + 1) {
            const result = await knex.raw(`
                SELECT *, DATE_FORMAT(created_at,'%Y-%m-%d %H:%i:%s') AS created_at FROM v_report_interaction 
                WHERE DATE_FORMAT(created_at,'%Y-%m-%d') >= '${date_start}' AND DATE_FORMAT(created_at,'%Y-%m-%d') <= '${date_end}'
                ${filter_by_customer} ORDER BY id DESC
            `);

            response.ok(res, result[0]);
        }
        else {
            res.json({
                'status': 204,
                'data': `Max range ${max} days.`
            });
            res.end();
        }
    }
    catch (error) {
        logger('report/interaction_export', error);
        res.status(500).end();
    }
}

module.exports = { report_interaction_grid, report_interaction_export }