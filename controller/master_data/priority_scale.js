const knex = require('../../config/db_connect');
const response = require('../../helper/json_response');

const index = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { data } = req.query;
        
        let result;
        if (data == 'active') {
            result = await knex('priority_scale').where({ active: '1' }).orderBy('id', 'asc');
        }
        else {
            result = await knex('priority_scale').orderBy('id', 'asc');
        }
        response.ok(res, result);
    }
    catch (error) {
        logger('priority_scale/index', error);
        res.status(500).end();
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { id } = req.params;
        const result = await knex('priority_scale').where({ id }).first();
        response.ok(res, result);
    }
    catch (error) {
        logger('priority_scale/show', error);
        res.status(500).end();
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const {
            priority_scale,
            description,
            active,
            created_by,
            created_at = knex.fn.now(),
        } = req.body;

        await knex('priority_scale')
            .insert([{
                priority_scale,
                description,
                active,
                created_by,
                created_at,
            }]);
        response.ok(res, 'success insert');
    }
    catch (error) {
        logger('priority_scale/store', error);
        res.status(500).end();
    }
}

const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        const {
            id,
            priority_scale,
            description,
            active,
            updated_by,
            updated_at = knex.fn.now(),
        } = req.body;

        await knex('priority_scale')
            .update({
                priority_scale,
                description,
                active,
                updated_by,
                updated_at
            })
            .where({ id });
        response.ok(res, 'success update');
    }
    catch (error) {
        logger('priority_scale/update', error);
        res.status(500).end();
    }
}

const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        const { id } = req.params;
        const result = await knex('priority_scale').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        logger('priority_scale/destroy', error);
        res.status(500).end();
    }
}

module.exports = {
    index,
    show,
    store,
    update,
    destroy,
}