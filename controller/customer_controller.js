'use strict';
const knex = require('../config/db_connect');
const date = require('date-and-time');
const { auth_jwt_bearer } = require('../middleware');
const response = require('../helper/json_response');
const { insert_channel_customer, destroy_channel_customer } = require('./customer_channel_controller');

const index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY customer_id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const customers = await knex.raw(`
            SELECT * FROM customers WHERE (syncronized_to IS NULL OR syncronized_to='')
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from customers WHERE (syncronized_to IS NULL OR syncronized_to='') ${filtering}`);
        response.data(res, customers[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'customer/index');
    }
}

const data_registered = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY customer_id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const customers = await knex.raw(`
            SELECT * FROM customers WHERE (syncronized_to IS NULL OR syncronized_to='') AND status='Registered'
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from customers WHERE (syncronized_to IS NULL OR syncronized_to='') AND status='Registered' ${filtering}`);
        response.data(res, customers[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'customer/data_registered');
    }
}

const data_grid = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        const { skip, take, sort, filter } = req.query;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY customer_id ASC';
        if (datasort) {
            datasort = JSON.parse(datasort)
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            datafilter = JSON.parse(datafilter)
            filtering = `WHERE ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const customers = await knex.raw(`
            SELECT * FROM customers
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from customers ${filtering}`);
        response.data(res, customers[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'customer/index');
    }
}

const show = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { customer_id } = req.params;
        const query_channel = await knex.raw(`SELECT * FROM customer_channels WHERE customer_id='${customer_id}' OR value_channel='${customer_id}' limit 1`);
        const channel = query_channel[0][0];
        if (channel) {
            const result = await knex.raw(`SELECT * FROM customers WHERE customer_id='${channel.customer_id}' limit 1`);
            response.ok(res, result[0]);
        }
        else {
            const result = await knex.raw(`SELECT * FROM customers WHERE customer_id='${customer_id}' OR phone_number='${customer_id}' OR email='${customer_id}' limit 1`);
            response.ok(res, result[0]);
        }
    }
    catch (error) {
        response.error(res, error.message, 'customer/show');
    }
}

const store = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);

        const now = new Date();
        const customer_id = date.format(now, 'YYMMDDHHmmSS');
        const {
            name,
            email,
            no_ktp,
            // birth,
            gender,
            phone_number,
            address
        } = req.body;

        const check_email = await knex('customers').where({ email });
        if (check_email.length > 0) return response.created(res, `email : ${email} - already exists.`);
        const check_phone = await knex('customers').where({ phone_number });
        if (check_phone.length > 0) return response.created(res, `phone_number : ${phone_number} - already exists.`);

        if (check_email.length === 0 && check_phone.length === 0) {
            await knex('customers')
                .insert([{
                    customer_id,
                    name,
                    email,
                    no_ktp,
                    // birth,
                    gender,
                    phone_number,
                    address,
                    status: 'Registered',
                    source: 'Manual',
                    created_at: knex.fn.now()
                }]);

            await insert_channel_customer({ customer_id, value_channel: email, flag_channel: 'Email' });
            await insert_channel_customer({ customer_id, value_channel: phone_number, flag_channel: 'Phone' });
            const getcustomer = await knex('customers').where({ email }).first();
            response.ok(res, getcustomer);
        }
    }
    catch (error) {
        response.error(res, error.message, 'customer/store');
    }
}


const update = async function (req, res) {
    try {
        if (req.method !== 'PUT') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            customer_id,
            name,
            email,
            no_ktp,
            // birth,
            gender,
            phone_number,
            address
        } = req.body;
        const check_email = await knex('customers').where({ email }).whereNot({ customer_id });
        if (check_email.length > 0) return response.created(res, `email : ${email} - already exists.`);
        const check_phone = await knex('customers').where({ phone_number }).whereNot({ customer_id });
        if (check_phone.length > 0) return response.created(res, `phone_number : ${phone_number} - already exists.`);

        if (check_email.length === 0 && check_phone.length === 0) {
            await knex('customers')
                .update({
                    name,
                    email,
                    no_ktp,
                    // birth,
                    gender,
                    phone_number,
                    address,
                    status: 'Registered',
                    updated_at: knex.fn.now()
                })
                .where({ customer_id });

            await insert_channel_customer({ customer_id, value_channel: email, flag_channel: 'Email' });
            await insert_channel_customer({ customer_id, value_channel: phone_number, flag_channel: 'Phone' });
            const getData = await knex('customers').where({ customer_id }).first();
            response.ok(res, getData);
        }
    }
    catch (error) {
        response.error(res, error.message, 'customer/update');
    }
}


const destroy = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { customer_id } = req.params;
        const delData = await knex('customers').where({ customer_id }).del();
        await destroy_channel_customer({ customer_id });
        response.ok(res, delData);
    }
    catch (error) {
        response.error(res, error.message, 'customer/destroy');
    }
}

const insert_customer_sosmed = async function (req) {
    try {
        const { name, email } = req;
        const now = new Date();
        const customer_id = date.format(now, 'YYMMDDHHmmSS');
        const check_data = await knex('customers').where('email', email);

        if (check_data.length === 0) {
            await knex('customers')
                .insert([{
                    customer_id: customer_id,
                    name: name,
                    email: email,
                    source: 'chat',
                    status: 'Initialize',
                    created_at: knex.fn.now()
                }]);
        }
    }
    catch (error) {
        response.error(res, error.message, 'customer/insert_customer_sosmed');
    }
}

const customer_journey = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { customer_id } = req.params;
        const journey = await knex.raw(`SELECT ticket_number,customer_id,thread_id,status,ticket_source,source_information,account_id,subject,date_create FROM view_tickets WHERE customer_id='${customer_id}' OR customer_id in (SELECT syncronized_to FROM customers WHERE customer_id='${customer_id}') ORDER BY date_create DESC`);

        for (let i = 0; i < journey[0].length; i++) {
            journey[0][i].time = date.format(journey[0][i].date_create, 'HH:mm', true)
            journey[0][i].date = date.format(journey[0][i].date_create, 'dddd, DD MMMM YYYY')
        }

        response.ok(res, journey[0]);
    }
    catch (error) {
        response.error(res, error.message, 'customer/customer_journey');
    }
}

const data_export = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const customers = await knex('customers');
        response.ok(res, customers);
    }
    catch (error) {
        response.error(res, error.message, 'customer/data_export');
    }
}

const update_syncronize_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { customer_primary, customer_sync } = req.body;

        if (customer_primary && customer_sync) {
            const result = await knex.raw(`CALL sp_customer_syncronize('${customer_primary}','${customer_sync}')`)
            response.ok(res, result[0][0]);
        }
        else {
            response.error(res, 'failed, syncronize check parameter.', 'customer/syncronize_customer');
        }
    }
    catch (error) {
        response.error(res, error.message, 'customer/syncronize_customer');
    }
}

const update_unsyncronize_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        const { customer_primary, customer_sync } = req.body;

        if (customer_primary && customer_sync) {
            const result = await knex.raw(`CALL sp_customer_unsyncronize('${customer_primary}','${customer_sync}')`)
            response.ok(res, result[0][0]);
        }
        else {
            response.error(res, 'failed, syncronize check parameter.', 'customer/unsyncronize_customer');
        }
    }
    catch (error) {
        response.error(res, error.message, 'customer/unsyncronize_customer');
    }
}

const data_syncronize_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, syncronized_to } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';
        let sync_to = syncronized_to ?? '';

        let orderby = 'ORDER BY customer_id ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }
        let syncronized = '';
        if (sync_to) {
            syncronized = `AND syncronized_to='${syncronized_to}'`;
        }

        const customers = await knex.raw(`
            SELECT * FROM customers WHERE (syncronized_to IS NOT NULL OR syncronized_to!='')
            ${syncronized}
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`SELECT COUNT(*) AS total from customers WHERE (syncronized_to IS NOT NULL OR syncronized_to!='') ${syncronized} ${filtering}`);
        response.data(res, customers[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error.message, 'customer/index');
    }
}


module.exports = {
    index,
    data_registered,
    data_grid,
    show,
    store,
    update,
    destroy,
    insert_customer_sosmed,
    customer_journey,
    data_export,
    update_syncronize_customer,
    update_unsyncronize_customer,
    data_syncronize_customer,
}
