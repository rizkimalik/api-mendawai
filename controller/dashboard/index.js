const dash_sosialmedia = require('./dash_sosialmedia');
const dash_ticket = require('./dash_ticket');
const dash_sentiment = require('./dash_sentiment');

module.exports = {
    dash_sosialmedia,
    dash_ticket,
    dash_sentiment,
}