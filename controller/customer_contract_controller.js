'use strict';
const knex = require('../config/db_connect');
const date = require('date-and-time');
const { auth_jwt_bearer } = require('../middleware');
const response = require('../helper/json_response');

exports.customer_contract_index = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY po_date_open ASC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        const customers = await knex.raw(`
            SELECT a.*,b.name as customer_name
            FROM customer_contract AS a
            LEFT JOIN customers AS b ON a.customer_id=b.customer_id
            ${filtering}
            ${orderby}
            LIMIT ${datatake} OFFSET ${dataskip}
        `);
        const total = await knex.raw(`
            SELECT COUNT(*) AS total 
            FROM customer_contract AS a
            LEFT JOIN customers AS b ON a.customer_id=b.customer_id 
            ${filtering}
        `);
        response.data(res, customers[0], total[0][0].total);
    }
    catch (error) {
        response.error(res, error, 'customer_contract/customer_contract_index');
    }
}

exports.customer_contract_detail = async function (req, res) {
    try {
        if (req.method !== 'GET') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { project_number } = req.params;
        const contract = await knex('customer_contract').where({ project_number }).first();
        response.ok(res, contract);
    }
    catch (error) {
        response.error(res, error, 'customer_contract/customer_contract_detail');
    }
}

exports.contract_by_customer = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { customer_id } = req.body;
        const contract = await knex('customer_contract').where({ customer_id });
        response.ok(res, contract);
    }
    catch (error) {
        response.error(res, error, 'customer_contract/contract_by_customer');
    }
}

exports.customer_contract_insert = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            customer_id,
            project_number,
            scope,
            product,
            product_system,
            po_number,
            description,
            po_date_open,
            po_date_start,
            po_date_end,
        } = req.body;

        const result = await knex('customer_contract')
            .insert({
                customer_id,
                project_number,
                scope,
                product,
                product_system,
                po_number,
                description,
                po_date_open,
                po_date_start,
                po_date_end,
            });
        response.ok(res, result[0].message);
    }
    catch (error) {
        response.error(res, error.message, 'customer_contract/customer_contract_insert');
    }
}

exports.customer_contract_update = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const {
            id,
            customer_id,
            project_number,
            scope,
            product,
            product_system,
            po_number,
            description,
            po_date_open,
            po_date_start,
            po_date_end,
        } = req.body;

        const result = await knex('customer_contract')
            .update({
                customer_id,
                project_number,
                scope,
                product,
                product_system,
                po_number,
                description,
                po_date_open,
                po_date_start,
                po_date_end,
            })
            .where({ id });
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'customer_contract/customer_contract_update');
    }
}

exports.customer_contract_delete = async function (req, res) {
    try {
        if (req.method !== 'DELETE') return res.status(405).end('Method not Allowed');
        auth_jwt_bearer(req, res);
        const { id } = req.params;
        const result = await knex('customer_contract').where({ id }).del();
        response.ok(res, result);
    }
    catch (error) {
        response.error(res, error.message, 'customer_contract/customer_contract_delete');
    }
}