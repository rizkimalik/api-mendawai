module.exports = function (socket) {
    socket.on('transfer-assign-agent', (data) => {
        socket.to(data.agent_handle).emit('return-transfer-assign-agent', data);
    });

    socket.on('send-message-error', (data) => {
        socket.to(data.agent_handle).emit('return-message-error', data);
    });

    // Join a room
    socket.on('join-room', (room) => {
        socket.join(room);
        console.log(`User[${socket.id}] joined room: ${room}`);
    });

    // Leave a room
    socket.on('leave-room', (room) => {
        socket.leave(room);
        console.log(`User[${socket.id}] left room: ${room}`);
    });

    socket.on('return-notification', (data) => {
        socket.broadcast.emit('return-notification', data); //notused
    });

}