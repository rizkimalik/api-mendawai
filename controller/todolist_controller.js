'use strict';
const knex = require('../config/db_connect');
const { auth_jwt_bearer } = require('../middleware');
const logger = require('../helper/logger');
const response = require('../helper/json_response');

const total_ticket = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { department_id, user_create, user_level } = req.body;

        let tickets;
        if (user_level === 'L1') {
            tickets = await knex.raw(`SELECT status,icon, (select count(ticket_number) FROM view_tickets where status=status.status) as total FROM status WHERE active='1'`);
            // tickets = await knex.raw(`SELECT status,icon, (select count(ticket_number) FROM view_tickets where status=status.status AND department_id='${department_id}' AND (user_create='${user_create}' OR ticket_position='L1')) as total FROM status WHERE active='1'`);
        }
        else if (user_level === 'L2') {
            tickets = await knex.raw(`
                SELECT status,icon, (select count(ticket_number) FROM view_tickets where status=status.status 
                AND (dispatch_department='${department_id}' OR department_id='${department_id}') AND ticket_number IN (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND (dispatch_to_layer='L1' OR dispatch_to_layer='L2'))
                ) as total FROM status WHERE active='1'
            `);
        }
        else if (user_level === 'L3') {
            tickets = await knex.raw(`
                SELECT status,icon, (select count(ticket_number) FROM view_tickets 
                where status=status.status AND (dispatch_department='${department_id}' OR department_id='${department_id}')
                AND ticket_number IN (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND (dispatch_to_layer='L1' OR dispatch_to_layer='L2' OR dispatch_to_layer='L3'))
                ) as total FROM status WHERE active='1'
            `);
        }
        else {
            //? user_level = admin
            tickets = await knex.raw(`SELECT status,icon, (select count(ticket_number) FROM view_tickets where status=status.status) as total FROM status WHERE active='1'`);
        }

        if (process.env.DB_CONNECTION === 'mysql') {
            response.ok(res, tickets[0]);
        }
        else {
            response.ok(res, tickets);
        }
    }
    catch (error) {
        logger('todolist/total_ticket', error);
        res.status(500).end();
    }
}

const data_ticket = async function (req, res) {
    try {
        if (req.method !== 'POST') return res.status(405).end();
        auth_jwt_bearer(req, res);
        const { skip, take, sort, filter, department_id, user_create, user_level, status } = req.body;
        const dataskip = skip ?? 0;
        const datatake = take ?? 10;
        let datasort = sort ?? '';
        let datafilter = filter ?? '';

        let orderby = 'ORDER BY date_create DESC';
        if (datasort) {
            let desc = datasort[0].desc === true ? 'desc' : 'asc';
            orderby = `ORDER BY ${datasort[0].selector} ${desc}`;
        }

        let filtering = '';
        if (datafilter) {
            // datafilter = JSON.parse(datafilter)
            filtering = `AND ${datafilter[0]} LIKE '%${datafilter[2]}%'`;
        }

        let param_ticket, param_total;
        if (user_level === 'L1') {
            param_ticket = `WHERE status='${status}' ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE status='${status}' ${filtering}`;
            // param_ticket = `WHERE status='${status}' AND department_id='${department_id}' ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            // param_total = `WHERE status='${status}' AND department_id='${department_id}' AND (user_create='${user_create}' OR ticket_position='L1') ${filtering}`;
        }
        else if (user_level === 'L2') {
            param_ticket = `WHERE status='${status}' AND (dispatch_department='${department_id}' OR department_id='${department_id}') AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND (dispatch_to_layer='L1' OR dispatch_to_layer='L2')) ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE status='${status}' AND (dispatch_department='${department_id}' OR department_id='${department_id}') AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND (dispatch_to_layer='L1' OR dispatch_to_layer='L2')) ${filtering}`;
        }
        else if (user_level === 'L3') {
            param_ticket = `WHERE status='${status}' AND (dispatch_department='${department_id}' OR department_id='${department_id}') AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND (dispatch_to_layer='L1' OR dispatch_to_layer='L2' OR dispatch_to_layer='L3')) ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE status='${status}' AND (dispatch_department='${department_id}' OR department_id='${department_id}') AND ticket_number in (SELECT ticket_number FROM ticket_interactions WHERE ticket_number=view_tickets.ticket_number AND (dispatch_to_layer='L1' OR dispatch_to_layer='L2' OR dispatch_to_layer='L3')) ${filtering}`;
        }
        else {
            //? user_level = admin
            param_ticket = `WHERE status='${status}' ${filtering} ${orderby} LIMIT ${datatake} OFFSET ${dataskip}`;
            param_total = `WHERE status='${status}' ${filtering}`;
        }

        const tickets = await knex.raw(`SELECT *, DATE_FORMAT(date_create,'%Y-%m-%d %H:%i:%s') AS date_create, DATE_FORMAT(last_response_at,'%Y-%m-%d %H:%i:%s') AS last_response_at FROM view_tickets ${param_ticket}`);
        const total = await knex.raw(`SELECT COUNT(*) AS total from view_tickets ${param_total}`);

        const data_ticket = tickets[0];
        for (let i = 0; i < data_ticket.length; i++) {
            let attachment = await knex('ticket_attachments').count('* as total_attachment').where({ ticket_number: data_ticket[i].ticket_number }).first();
            data_ticket[i].attachment = attachment ? attachment.total_attachment : 0;
        }
        
        response.data(res, tickets[0], total[0][0].total);
    }
    catch (error) {
        logger('todolist/data_ticket', error);
        res.status(500).end();
    }
}

module.exports = {
    total_ticket,
    data_ticket,
}