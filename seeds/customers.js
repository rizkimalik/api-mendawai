
exports.seed = async function (knex) {
    let fakeCustomers = [];
    for (let i = 1; i <= 10000000; i += 1) {
        fakeCustomers.push({
            customer_id: `8002030200${i}`,
            name: `customer${i}`,
            email: `customer${i}@gmail.com`,
            no_ktp: `10008219030005${i}`,
            birth: knex.fn.now(),
            gender: 'Male',
            phone_number: `08219030005${i}`,
            address: `jl raya jakarta no ${i}`,
            status: 'Initialize',
            source: 'Manual',
            created_at: knex.fn.now()
        });

      if (i % 1000 === 0) {
        await knex('customers').insert(fakeCustomers);
        fakeCustomers = [];
      }
    }
}

// yarn knex seed:run --specific=customers.js
